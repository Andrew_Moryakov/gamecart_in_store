﻿using UnityEngine;

namespace Assets.Scripts
{
	public class FoodScript: MonoBehaviour
	{
		private Collider2D _collider2D;
		private float _score;
		public float Speed = 2.5f;
		public float Force = 1000;
		public GameObject[] Prefabs = new GameObject[0];
		public float FoodMass = 0.02f;


		void Awake()
		{
			_collider2D = GetComponent<Collider2D>();
		}

		void Start()
		{
			//Устанавливаем рандомную картинку пустому спрайту
			SetRandomSprite();
		}
		
		void Update()
		{
			MoveToDown();
		}

		void OnTriggerEnter2D(Collider2D collider)
		{
			CollisionWithCart(collider);
			SetRandomSprite();
			ToStartPosition();
		}

		void CollisionWithCart(Collider2D collider)
		{
			if (collider.name == "Cart")
			{
				ScoreInc();
				GameObject cartGameObject = GameObject.Find("Cart");
				cartGameObject.GetComponent<Rigidbody2D>().mass += FoodMass;
			}
			else
			{
				DecInc();
			}
		}

		void ScoreInc()
		{
			_score++;
		}
		void DecInc()
		{
			_score--;
		}

		void MoveToDown()
		{
			_collider2D.transform.Translate(down * Time.deltaTime * Speed) ;//.position = newPos;
			Vector3 newVect;
			newVect.x = _collider2D.transform.position.x;
			newVect.y = _collider2D.transform.position.y;
			newVect.z = -1.6f;

			_collider2D.transform.position = newVect;
			
		}
		Vector3 down = new Vector3(0f, -1f, -1.6f);
		Vector2 GetRandomStartPos()
		{
			float leftSidePos = -2.5f;
			float rightSidePos = 2.5f;

			float randomXpose = (int)Random.Range(leftSidePos, rightSidePos);
			return new Vector3(randomXpose, 7, -1.6f);

		}

		void ToStartPosition()
		{
			var newRandStartPos = GetRandomStartPos();
			gameObject.transform.position = newRandStartPos;
		}


		void SetRandomSprite()
		{
			int randomPrefab = Random.Range(0, Prefabs.Length);
			string prefabRandomName = Prefabs[randomPrefab].name;
			string pathToRandomPrefab = string.Format("Assets/Prefabs/Foods/{0}.prefab", prefabRandomName);

			var prefabObjects = (UnityEditor.AssetDatabase.LoadAllAssetsAtPath(pathToRandomPrefab));
			SpriteRenderer spriteRenderOfPrefab = null;

			foreach (var obj in prefabObjects)
			{
				spriteRenderOfPrefab = obj as SpriteRenderer;
				if (spriteRenderOfPrefab != null)
				{
					break;
				}
			}

			if (spriteRenderOfPrefab != null)
			{
				gameObject.GetComponent<SpriteRenderer>().sprite = spriteRenderOfPrefab.sprite;
			}
		}

	}
}
