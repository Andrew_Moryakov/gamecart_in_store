﻿using UnityEngine;

namespace Assets.Scripts
{
	public class CartScript: MonoBehaviour
	{
		public float Speed = 200f;
		private Rigidbody2D _rigidbody2D;
		private Vector3 _newPositionByMouse;

		void Awake()
		{
			_rigidbody2D = GetComponent<Rigidbody2D>();
		}
	
		void Update()
		{
			MouseCollisionColider();
			MoveKeyLeft();
			MoveKeyRight();

			if (Input.GetKeyDown(KeyCode.Space))
			{
				_rigidbody2D.AddForce(Vector2.up * Speed * Time.deltaTime, ForceMode2D.Impulse);
			}

		} 

		void MouseCollisionColider()
		{
			if (Input.GetMouseButton(0))
			{
						MoveByMouse();
			}
		}

		
		void MoveByMouse()
		{
			_newPositionByMouse.x = Camera.main.ScreenToWorldPoint(Input.mousePosition).x;
			_newPositionByMouse.y = _rigidbody2D.transform.position.y;
			_newPositionByMouse.z = _rigidbody2D.transform.position.z;

			_rigidbody2D.MovePosition(_newPositionByMouse);
		}

		void MoveKeyLeft()
		{
			if (Input.GetKey(KeyCode.A))
			{
				_rigidbody2D.AddForce(Vector2.left * Speed * Time.deltaTime, ForceMode2D.Force);
			}
		}

		void MoveKeyRight()
		{
			if (Input.GetKey(KeyCode.D))
			{
				_rigidbody2D.AddForce(Vector2.right * Speed * Time.deltaTime, ForceMode2D.Force);
			}
		}
	}
}